public class HelloJava {

  public static void main(String[] args){  
    welcomeParty(args);    
  }  

  private static void welcomeParty(String[] args){
    if(args.length < 1){
      System.out.println("Usage: java HelloWorld name");
    }else{
      System.out.println("Hello, " + args[0]);  
    }    
  }
  
}
