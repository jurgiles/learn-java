public class HelloJava {
  public static void main(String[] args){  
    //Create a method and call it. 
    if(args.length < 1){
      System.out.println("Usage: java HelloWorld name");
    }else{
      System.out.println("Hello, " + args[0]);  
    }    
  }  

  // 1 - Add arguments
  private static void welcomeParty(){
    // 2 - Move logic into here
  }  
}
