public class HelloJava {

  public static void main(String[] args){      
    // 1 - Move argument count here.
    // tip: you can use 'return' to return inside of a void function
    if(args.length < 1){
      System.out.println("Usage: java HelloWorld name");
      return;
    }

    // 2 - Declare a new greeter passing in a prefix message as an argument
    Greeter greeter = new Greeter("Hola, ");
    // 3 - Get the welcome greeting from the greeter.
    String greeting = greeter.welcome(args[0]);
    // 4 - print it to the screen
    System.out.println(greeting);
  }    
}