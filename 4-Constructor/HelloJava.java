public class HelloJava {

  public static void main(String[] args){      
    // 1 - Move argument count here.
    // tip: you can use 'return' to return inside of a void function

    // 2 - Declare a new greeter passing in a prefix message as an argument
    Greeter greeter = new Greeter();
    // 3 - Get the welcome greeting from the greeter.
    String greeting = null;
    // 4 - print it to the screen
    System.out.println(greeting);
  }    
  
}