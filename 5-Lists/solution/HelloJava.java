import java.util.List;
import java.util.Arrays;


public class HelloJava {

  public static void main(String[] args){      
    if(args.length < 1){
      System.out.println("Usage: java HelloWorld name");
      return;
    }

    // 1 - Create a list container from the args names
    List<String> names = Arrays.asList(args);

    Greeter greeter = new Greeter("Hola, ");
    
    // 2 - Update argument and return value
    List<String> greetings = greeter.welcome(names);
    
    for(String greeting : greetings){
      System.out.println(greeting);
    }
  }    
}