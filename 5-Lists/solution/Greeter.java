import java.util.List;
import java.util.ArrayList;

public class Greeter{
  private final String message; 

  public Greeter(String message){
    this.message = message; 
  }

  public List<String> welcome(List<String> names){
      List<String> welcomeMessages = new ArrayList<String>();

      for(String name : names){
        welcomeMessages.add(message + name);
      }

      return welcomeMessages;      
  }
}