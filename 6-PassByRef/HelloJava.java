import java.util.List;
import java.util.Arrays;


public class HelloJava {

  public static void main(String[] args){      
    if(args.length < 1){
      System.out.println("Usage: java HelloWorldSolution name");
      return;
    }

    // 1 - Construct as ArrayList, don't forget import
    List<String> names = Arrays.asList(args);

    Greeter greeter = new Greeter("Hola, ");
    
    List<String> greetings = greeter.welcome(names);
    
    for(String greeting : greetings){
      System.out.println(greeting);
    }

    // 2 - Add a separator message and repeat message 
  }    
}