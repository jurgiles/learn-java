public class HelloJava {
  public static void main(String[] args){  
    // 1 - Update to say include name from command line. 
    // hint: string concat is possible in Java with +

    // 2 - Update to print first arg if at least one argument is provided
    // otherwise print "Usage: java HelloWorld name"
    // hint: arrays have an attribute length
    System.out.println("Hello, ");
  }  
}
