public class Greeter{
  public void welcome(String[] args){
    if(args.length < 1){
      System.out.println("Usage: java HelloWorldSolution name");
    }else{
      System.out.println("Hello, " + args[0]);  
    }    
  }
}